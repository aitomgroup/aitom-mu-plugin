<?php

namespace AITOM\LoginScreen;

/**
 * Redirects search results from /?s=query to /search/query/, converts %20 to +
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-login-screen' );
 */

add_action( 'login_header', __NAMESPACE__ . '\\login_screen' );

function login_screen(){ ?>
    <style>

        body.login #login h1 { display: none }
        
        body.login #login h1 + .message,
        body.login #login h1 + #login_error { margin-top: 10px }

        body.login #login + #login { padding-top: 0 }

        /* `Aitom signature
        ----------------------------------------------------------------------------------------------------*/

        .ai-signature {
            display: inline-block;
            height: 52px;
            vertical-align: middle }

            .ai-signature,
            .ai-signature:focus {
                outline: none;

                    -webkit-box-shadow: none;
                    -moz-box-shadow: none;
                    box-shadow: none }

        .ai-signature__wrap {
            padding-bottom: 10px !important;
            text-align: center }

        .ai-signature__logo {
            height: 52px;
            overflow: visible;
            width: 151px /* height * 2.9 */ }

        .ai-signature__symbol__path {


                -webkit-transition: 1.8s;
                -webkit-transform: rotate(0);
                -webkit-transform-origin: 20px center;
                -moz-transition: 1.8s;
                -moz-transform: rotate(0);
                -moz-transform-origin: 20px center;
                -ms-transition: 1.8s;
                -ms-transform: rotate(0);
                -ms-transform-origin: 20px center;
                -o-transition: 1.8s;
                -o-transform: rotate(0);
                -o-transform-origin: 20px center;
                transition: 1.8s;
                transform: rotate(0);
                transform-origin: 20px center }

        .ai-signature__symbol__path,
        .ai-signature__text__path { fill: #4a3728 }

            .ai-signature:hover .ai-signature__symbol__path {


                    -webkit-transform: rotate(720deg);
                    -moz-transform: rotate(720deg);
                    -ms-transform: rotate(720deg);
                    -o-transform: rotate(720deg);
                    transform: rotate(720deg) }

    </style>

    <div id="login" class="ai-signature__wrap"><a class="ai-signature" title="Powered by AITOM" href="https://www.aitom.cz/">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100.994 34" class="ai-signature__logo"><g class="ai-signature__text"><path class="ai-signature__text__path" d="M50.137 12.632h4.426l3.888 9.133h-3.836l-.384-1.05h-3.76l-.384 1.05H46.25m6.113-6.37h-.026l-1.074 3.095h2.174l-1.074-3.095zM58.95 12.632h3.607v9.133H58.95zM66.766 14.934h-3.34v-2.302h10.285v2.302h-3.337v6.83h-3.607M80.298 12.312c2.188 0 6.242.154 6.242 4.886 0 4.733-4.054 4.886-6.242 4.886-2.187 0-6.242-.14-6.242-4.886 0-4.732 4.055-4.886 6.242-4.886m0 7.47c1.484 0 2.405-.73 2.405-2.584s-.92-2.584-2.405-2.584c-1.483 0-2.404.73-2.404 2.584 0 1.855.92 2.584 2.404 2.584M87.82 12.632h5.078l1.496 5.308h.026l1.496-5.308h5.078v9.133H97.72V15.24h-.026l-1.918 6.525h-2.738L91.12 15.24h-.026v6.525H87.82" class="ai-signature__text__path"/></g><g class="ai-signature__symbol"><path x="-20" y="-20" class="ai-signature__symbol__path" d="M28.495 9.434c.115-.23.14-.358.09-.512-.372-1.036-2.034-.793-2.674-.345-.562.384-.792.934-.792.934s-2.865 5.232-3.172 5.808c-.307.563.14.793.665.793L37.86 16.1h.294c.115 0 .78.04 1.164-.51 1.01-1.472-.64-2.214-1.612-2.253l-.946-.038h-10.36l2.007-3.633c-.025-.025-.025-.025.09-.23M16.586 7.17c-.14-.217-.243-.294-.396-.32-1.087-.192-1.7 1.37-1.637 2.15.05.677.41 1.15.41 1.15s3.12 5.09 3.452 5.64c.333.55.755.27 1.024-.19.267-.46 7.57-13.24 7.57-13.24l.142-.255c.05-.102.422-.652.128-1.266-.768-1.613-2.24-.55-2.75.28l-.5.806-5.154 8.992-2.162-3.543-.128-.205M7.287 13.144c-.256.012-.383.05-.486.166-.728.83.283 2.16.973 2.52.602.307 1.203.255 1.203.255s5.973 0 6.613.013c.638 0 .625-.5.37-.972-.256-.46-7.317-13.38-7.317-13.38l-.153-.255c-.052-.1-.333-.703-1.01-.767C5.7.543 5.84 2.348 6.276 3.218l.422.857 4.962 9.094-4.144-.026h-.23M11.125 24.58c-.116.23-.14.357-.077.51.37 1.036 2.034.793 2.673.346.55-.384.794-.934.794-.934l3.172-5.794c.307-.563-.14-.793-.665-.793l-15.246.012H1.48c-.115 0-.78-.038-1.164.525-1.01 1.47.64 2.213 1.612 2.25l.946.027h10.36l-2.007 3.63-.102.22M23.007 26.83c.14.217.243.294.397.332 1.087.18 1.7-1.368 1.637-2.148-.05-.678-.408-1.152-.408-1.152s-3.12-5.09-3.454-5.64c-.332-.55-.754-.27-1.023.18-.27.46-7.572 13.237-7.572 13.237l-.14.255c-.052.102-.423.652-.13 1.266.77 1.613 2.24.55 2.75-.28l.5-.806 5.155-8.992 2.15 3.543c.012 0 .012 0 .14.205M32.652 20.856c.256-.012.384-.05.486-.166.73-.818-.28-2.16-.972-2.52-.6-.307-1.203-.255-1.203-.255s-5.973-.013-6.613-.013c-.64 0-.626.51-.37.972.255.46 7.316 13.38 7.316 13.38l.154.255c.05.1.332.703 1.01.767 1.778.19 1.637-1.625 1.202-2.495l-.422-.844-4.963-9.094 4.145.025.23-.014" class="ai-signature__symbol__path"/></g></svg>
    </a></div>
<?php }