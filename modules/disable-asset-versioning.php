<?php

namespace AITOM\DisableAssetVersioning;

/**
 * Remove version query string from all styles and scripts
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-disable-asset-versioning' );
 */

add_filter( 'script_loader_src', __NAMESPACE__ . '\\remove_script_version', 15, 1 );
add_filter( 'style_loader_src', __NAMESPACE__ . '\\remove_script_version', 15, 1 );

function remove_script_version( $src ) {
    if ( !$src ) return false;
    
    $src_vars = parse_url( $src );
    if ( !empty( $src_vars ) && isset( $src_vars['query'] ) ) parse_str( $src_vars['query'], $src_vars );

    return !empty( $src_vars ) && isset( $src_vars['ver'] ) ? esc_url( add_query_arg( 'ver', md5( $src_vars['ver'] ), $src ) ) : $src;
}