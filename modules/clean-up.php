<?php

namespace AITOM\CleanUp;

/**
 * Clean up wp_head()
 *
 * Remove unnecessary <link>'s
 * Remove inline CSS and JS from WP emoji support
 * Remove inline CSS used by Recent Comments widget
 * Remove inline CSS used by posts with galleries
 * Remove self-closing tag
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-clean-up' );
 */

add_action( 'init', __NAMESPACE__ . '\\head_cleanup' );

function head_cleanup() {
    // Remove the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    // Removes feed links
    remove_action( 'wp_head', 'feed_links', 2 );
    
    // Remove comments feed links
    add_action( 'wp_head', 'ob_start', 1, 0 );
    add_action( 'wp_head', function() {
        $pattern = '/.*' . preg_quote( esc_url( get_feed_link( 'comments_' . get_default_feed() ) ), '/' ) . '.*[\r\n]+/';
      
        echo preg_replace( $pattern, '', ob_get_clean() );
    }, 3, 0 );
    
    // Remove the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // Remove the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // Remove prev link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // Remove start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // Remove the index page link
    remove_action( 'wp_head', 'index_rel_link' );
    // Remove relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
    // Remove the XHTML generator that is generated on the wp_head hook, WP version
    remove_action( 'wp_head', 'wp_generator' );
    // Remove shortlink of current url
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
    
    // Disable emoji
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content', 'convert_smilies', 20 );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'emoji_svg_url', '__return_false' );
    
    // Disable oEmbed Discovery Links
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );
    // Disable REST API link tag
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    // Disable REST API link in HTTP headers
    remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

    // Disable style of recent comments widget
    add_filter( 'show_recent_comments_widget_style', '__return_false' );
}

// Remove the WordPress version from RSS feeds
add_filter( 'the_generator', '__return_false' );

// Clean up output of stylesheet <link> tags
add_filter( 'style_loader_tag', __NAMESPACE__ . '\\clean_style_tag' );

function clean_style_tag( $input ) {
    preg_match_all( "!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches );
    
    if ( empty( $matches[2] ) ) return $input;
  
    // Only display media if it is meaningful
    $media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';
    
    return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
}

// Clean up output of <script> tags
add_filter( 'script_loader_tag', __NAMESPACE__ . '\\clean_script_tag' );

function clean_script_tag( $input ) {
    $input = str_replace( "type='text/javascript' ", '', $input );
    
    return str_replace( "'", '"', $input );
}

// Modify body classes
add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

function body_class( $classes ) {
    // Add post/page slug if not present
    if ( is_singular() || is_page() && !is_front_page() ) {
        global $post;
        if ( !empty( $post ) && isset( $post->post_name ) && !in_array( $post->post_name, $classes ) ) $classes[] = $post->post_name;
    }

    // Remove unnecessary classes
    $remove_classes = [
        'page-template',
        'page-template-default',
        'post-template',
        'post-template-default'
    ];
    
    $classes = array_diff( $classes, $remove_classes );
    
    $classes = array_filter( $classes, function( $class ) {
        if (
            strpos( $class, '-id-' )
            ||
            0 === strpos( $class, 'postid-' )
            ||
            0 === strpos( $class, 'term-' )
            ||
            0 === strpos( strrev( $class ), strrev( '-php' ) )
        ) {
            return false;
        }
        
        return true;
    });

    return $classes;
}

// Wrap embedded media as suggested by Readability
// @link https://gist.github.com/965956
add_filter( 'embed_oembed_html', __NAMESPACE__ . '\\embed_wrap' );

function embed_wrap( $cache ) {
    return '<div class="' . apply_filters( 'ai-filter-embed-wrap-class', 'embed' )  . '">' . $cache . '</div>';
}

// Remove unnecessary dashboard widgets
// @link http://www.deluxeblogtips.com/2011/01/remove-dashboard-widgets-in-wordpress.html
add_action( 'admin_init', __NAMESPACE__ . '\\remove_dashboard_widgets' );

function remove_dashboard_widgets() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
}

// Remove unnecessary self-closing tags
add_filter( 'get_avatar', __NAMESPACE__ . '\\remove_self_closing_tags' ); // <img />
add_filter( 'comment_id_fields', __NAMESPACE__ . '\\remove_self_closing_tags' ); // <input />
add_filter( 'post_thumbnail_html', __NAMESPACE__ . '\\remove_self_closing_tags' ); // <img />

function remove_self_closing_tags( $input ) {
    return str_replace( ' />', '>', $input );
}

// Don't return the default description if it hasn't been changed
add_filter( 'bloginfo', __NAMESPACE__ . '\\remove_default_description', 10, 2 );
add_filter( 'get_bloginfo_rss', __NAMESPACE__ . '\\remove_default_description', 10, 2 );

function remove_default_description( $bloginfo, $show ) {
    if ( is_admin() ) return $bloginfo;
    
    if ( $show == 'version' ) return '';
    
    if ( $show != 'description' ) return $bloginfo;
    
    $default_taglines = apply_filters( 'ai-filter-default-taglines', [ 'Just another WordPress site', 'Další web používající WordPress' ] );
    
    return ( in_array( $bloginfo, $default_taglines ) ) ? '' : $bloginfo;
}