<?php

namespace AITOM\StageSwitcher;

/**
 * Switch between different environments from the admin bar
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-stage-switcher' );
 */

add_action( 'admin_bar_menu', __NAMESPACE__ . '\\admin_bar_stage_switcher' );

function admin_bar_stage_switcher( $admin_bar ) {
    if ( !defined( 'ENVIRONMENTS' ) || !defined( 'WP_ENV' ) || !apply_filters( 'ai-filter-stage-switcher-visibility', current_user_can( 'administrator' ) ) ) return;

    $stages = unserialize( ENVIRONMENTS );
    $current_stage = WP_ENV;

    foreach( $stages as $stage => $url ) {
        if ( $stage === $current_stage ) continue;

        if ( is_multisite() && defined( 'SUBDOMAIN_INSTALL' ) && SUBDOMAIN_INSTALL && !is_main_site() ) {
            $url = multisite_url( $url ) . $_SERVER['REQUEST_URI'];
        } else {
            $url .= $_SERVER['REQUEST_URI'];
        }

        $admin_bar->add_menu( [
            'id'     => 'environment',
            'parent' => 'top-secondary',
            'title'  => ucwords( $current_stage ),
            'href'   => '#'
        ] );

        $admin_bar->add_menu( [
            'id'     => "stage_$stage",
            'parent' => 'environment',
            'title'  => ucwords( $stage ),
            'href'   => $url
        ] );
    }
}

add_action( 'wp_before_admin_bar_render', __NAMESPACE__ . '\\admin_css' );
    
function admin_css() { ?>
    <style>
        #wp-admin-bar-environment > a:before {
            content: "\f177";
            top: 2px;
        }
    </style>
<?php }

function multisite_url( $url ) {
    $stage_url = parse_url( $url );
    $current_url = parse_url( get_home_url( get_current_blog_id() ) );
    
    $stage_subdomain = array_shift( ( explode('.', $stage_url['host'] ) ) );
    $current_subdomain = array_shift( ( explode('.', $current_url['host'] ) ) );
    
    return implode( '', [
        $stage_url['scheme'] . '://',
        str_replace( $stage_subdomain, $current_subdomain, $stage_url['host'] ),
        $stage_url['path'] . $current_url['path'] . $_SERVER['REQUEST_URI']
    ] );
}