<?php

namespace AITOM\Nav;

use AITOM\Helpers;

/**
 * Cleaner walker for wp_nav_menu()
 *
 * Walker_Nav_Menu (WordPress default) example output:
 *   <li id="menu-item-8" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8"><a href="/">Home</a></li>
 *   <li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9"><a href="/sample-page/">Sample Page</a></l
 *
 * NavWalker example output:
 *   <li class="menu-home"><a href="/">Home</a></li>
 *   <li class="menu-sample-page"><a href="/sample-page/">Sample Page</a></li>
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-clean-walker' );
 */

class CleanNavWalker extends \Walker_Nav_Menu {
    private $cpt; // Boolean, if is current post a custom post type
    private $archive; // Save archive page link of current post type

    public function __construct() {
        add_filter( 'nav_menu_css_class', [ $this, 'item_classes' ], 10, 2 );
        add_filter( 'nav_menu_item_id', '__return_null' );
        $cpt           = get_post_type();
        $this->cpt     = in_array( $cpt, get_post_types( [ '_builtin' => false ] ) );
        $this->archive = get_post_type_archive_link( $cpt );
    }

    public function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
    
        $element->is_subitem = ( ( !empty( $children_elements[$element->ID] ) && ( ( $depth + 1 ) < $max_depth || ( $max_depth === 0 ) ) ) );

        if ( $element->is_subitem ) {
            foreach ( $children_elements[$element->ID] as $child ) {
                if ( $child->current_item_parent || Helpers\url_compare( $this->archive, $child->url ) ) $element->classes[] = 'active';
            }
        }

        $element->is_active = ( !empty( $element->url ) && strpos( $this->archive, $element->url ) );

        if ( $element->is_active ) $element->classes[] = 'active';

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    public function item_classes($classes, $item) {
        
        // Fix core `active` behavior for custom post types
        if ( $this->cpt ) {
            $classes = str_replace( 'current_page_parent', '', $classes );

            if ( Helpers\url_compare( $this->archive, $item->url ) ) $classes[] = 'active';
        }

        // Remove most core classes
        $classes = preg_replace( '/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes );
        $classes = preg_replace( '/^((menu|page)[-_\w+]+)+/', '', $classes );

        // Re-add core `menu-item` class
        $classes[] = 'menu-item';

        // Re-add core `menu-item-has-children` class on parent elements
        if ( $item->is_subitem ) $classes[] = 'menu-item-has-children';

        // Add `menu-<slug>` class
        $classes[] = 'menu-' . sanitize_title( $item->title );

        $classes = array_unique( $classes );
        $classes = array_map( 'trim', $classes );
    
        return array_filter( $classes );
        
    }
    
    public function check_current_item( $classes ) {
        return preg_match( '/(current[-_])|active/', $classes );
    }
}

// Remove the id="" attribute on nav menu items
add_filter( 'nav_menu_item_id', '__return_null' );

// Clean up wp_nav_menu_args
add_filter( 'wp_nav_menu_args', __NAMESPACE__ . '\\nav_menu_args' );

function nav_menu_args( $args = [] ) {
    $clean_menu_args = [];
    
    //Remove the container
    $clean_menu_args['container'] = false;

    if ( !$args['items_wrap'] ) $clean_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';

    if ( !$args['walker'] ) $clean_menu_args['walker'] = new CleanNavWalker();

    return array_merge( $args, $clean_menu_args );
}