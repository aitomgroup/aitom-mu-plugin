<?php

namespace AITOM\DisableTrackbacks;

/**
 * Disables trackbacks/pingbacks
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-disable-trackbacks' );
 */

// Disable pingback XMLRPC method
add_filter( 'xmlrpc_methods', __NAMESPACE__ . '\\modify_xmlrpc_method', 10, 1 );

function modify_xmlrpc_method( $methods ) {
    if ( isset( $methods['pingback.ping'] ) ) unset( $methods['pingback.ping'] );
    return $methods;
}

// Remove pingback header
add_filter( 'wp_headers', __NAMESPACE__ . '\\modify_headers', 10, 1 );

function modify_headers( $headers ) {
    if ( isset( $headers['X-Pingback'] ) ) unset($headers['X-Pingback']);
  
    return $headers;
}

// Remove trackback rewrite rule
add_filter('rewrite_rules_array', __NAMESPACE__ . '\\modify_rewrites');

function modify_rewrites( $rules ) {
    foreach ( $rules as $rule => $rewrite ) {
        if ( preg_match( '/trackback\/\?\$$/i', $rule ) ) unset( $rules[$rule] );
    }
    
    return $rules;
}

// Remove bloginfo('pingback_url')
add_filter( 'bloginfo_url', __NAMESPACE__ . '\\remove_pingback_url', 10, 2 );

function remove_pingback_url( $output, $show ) {
    if ( $show === 'pingback_url' ) $output = '';

    return $output;
}

// Disable XMLRPC call
add_action( 'xmlrpc_call', __NAMESPACE__ . '\\modify_xmlrpc' );

function modify_xmlrpc( $action ) {
    if ( $action === 'pingback.ping' ) wp_die( 'Pingbacks are not supported', 'Not Allowed!', [ 'response' => 403 ] );
}