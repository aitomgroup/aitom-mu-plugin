<?php

namespace AITOM\NiceSearch;

/**
 * Redirects search results from /?s=query to /search/query/, converts %20 to +
 *
 * @link http://txfx.net/wordpress-plugins/nice-search/
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-nice-search' );
 */

add_action( 'template_redirect', __NAMESPACE__ . '\\redirect' );

function redirect() {
    global $wp_rewrite;
    
    if ( !isset( $wp_rewrite ) || !is_object( $wp_rewrite ) || !$wp_rewrite->get_search_permastruct() ) return;
    
    if ( is_search() && !is_admin() && strpos( $_SERVER['REQUEST_URI'], "/{$wp_rewrite->search_base}/" ) === false && strpos( $_SERVER['REQUEST_URI'], '&' ) === false) {
        wp_redirect( get_search_link() );
        exit();
    }
}

add_filter( 'wpseo_json_ld_search_url', __NAMESPACE__ . '\\rewrite' );

function rewrite( $url ) {
    return str_replace( '/?s=', '/' . __( 'search', 'AITOM-MU' ) . '/', $url );
}