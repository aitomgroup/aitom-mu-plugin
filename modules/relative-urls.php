<?php

namespace AITOM\RelativeURLs;

use AITOM\Helpers;

/**
 * Root relative URLs
 *
 * WordPress likes to use absolute URLs on everything - let's clean that up.
 * Inspired by http://www.456bereastreet.com/archive/201010/how_to_make_wordpress_urls_root_relative/
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-relative-urls' );
 */

if ( is_admin() || isset( $_GET['sitemap'] ) || in_array( $GLOBALS['pagenow'], [ 'wp-login.php', 'wp-register.php' ] ) ) return;

$core_relative_urls = apply_filters( 'ai-filter-relative-urls', [
    'bloginfo_url',
    'the_permalink',
    'wp_list_pages',
    'wp_list_categories',
    'wp_get_attachment_url',
    'the_content_more_link',
    'the_tags',
    'get_pagenum_link',
    'get_comment_link',
    'month_link',
    'day_link',
    'year_link',
    'term_link',
    'the_author_posts_link',
    'script_loader_src',
    'style_loader_src'
] );

Helpers\add_filters( $core_relative_urls, 'AITOM\\Helpers\\core_relative_url');

add_filter( 'wp_calculate_image_srcset',  __NAMESPACE__ . '\\replace_url_to_relative' );

function replace_url_to_relative( $sources ) {
    foreach ( (array) $sources as $source => $src ) {
        $sources[$source]['url'] = \AITOM\Helpers\core_relative_url( $src['url'] );
    }
  
    return $sources;
}