<?php

namespace AITOM\HTMLCompression;

/**
 * Minify HTML output in WordPress
 * Forked from DVS: http://www.intert3chmedia.net/2011/12/minify-html-javascript-css-without.html
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-html-compression' );
 * You can also enable feature to hide all WordPress structure by adding:
 * add_theme_support( 'ai-hide-wp' );
 */

class AI_HTML_Compression {

    // Variables
    protected $html;

    public function __construct( $html ) {
        if ( !empty( $html ) ) {
            $this->parseHTML( $html );
        }
    }

    public function __toString() {
        return $this->html;
    }

    protected function bottomComment( $raw, $compressed ) {
        $raw = strlen( $raw );
        $compressed = strlen( $compressed );

        $savings = ( $raw - $compressed ) / $raw * 100;

        $savings = round( $savings, 2 );

        return '<!--HTML compressed, size saved ' . $savings . '%. From ' . $raw . ' bytes, now ' . $compressed . ' bytes-->';
    }
    
    protected function hideWP( $html ) {
        if ( !defined( 'CONTENT_DIR' ) || CONTENT_DIR != '/app' ) return $html;
        
        $html = strtr( $html, [
            'core/wp-includes' => 'app/libs',
            'core\/wp-includes' => 'app\/libs',
            'core/wp-admin/admin-ajax.php' => 'app/admin-ajax.php',
            'core\/wp-admin\/admin-ajax.php' => 'app\/admin-ajax.php'
        ] );
            
        return $html;
    }

    protected function minifyHTML( $html ) {

        // Settings
        $compress_css = false;
        $compress_js = false;
        $remove_comments = true;

        $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
        preg_match_all( $pattern, $html, $matches, PREG_SET_ORDER );

        $overriding = false;
        $raw_tag = false;

        // Variable reused for output
        $html = '';

        foreach ( $matches as $token ) {
            $tag = ( isset( $token['tag'] ) ) ? strtolower( $token['tag'] ) : null;

            $content = $token[0];

            if ( is_null( $tag ) ) {
                if ( !empty( $token['script'] ) ) {
                    $strip = $compress_js;
                }else if ( !empty( $token['style'] ) ) {
                    $strip = $compress_css;
                } else if ( $content == '<!--wp-html-compression no compression-->' ) {
                    $overriding = !$overriding;

                    // Don't print the comment
                    continue;
                } else if ( $remove_comments ) {
                    if ( !$overriding && $raw_tag != 'textarea' ) {
                        // Remove any HTML comments, except MSIE conditional comments
                        $content = preg_replace( '/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content );
                    }
                }
            } else {
                if ( $tag == 'pre' || $tag == 'textarea' ) {
                    $raw_tag = $tag;
                } else if ( $tag == '/pre' || $tag == '/textarea' ) {
                    $raw_tag = false;
                } else {
                    if ( $raw_tag || $overriding ) {
                        $strip = false;
                    } else {
                        $strip = true;

                        // Remove any empty attributes, except:
                        // action, alt, content, src
                        $content = preg_replace( '/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content );

                        // Remove any space before the end of self-closing XHTML tags
                        // JavaScript excluded
                        $content = str_replace( ' />', '/>', $content );
                    }
                }
            }

            if ( $strip ) {
                $content = $this->removeWhiteSpace( $content );
            }

            $html .= $content;
        }

        return $html;
    }

    public function parseHTML( $html ) {
        $this->html = $html;
        
        if ( ( !defined('WP_DEBUG') || !WP_DEBUG ) && defined('WP_ENV') && WP_ENV != 'development' ) {
            $this->html = $this->minifyHTML( $html );
            
            if ( current_theme_supports( 'ai-hide-wp' ) ) $this->html = $this->hideWP( $this->html );
            
            // Uncomment to show compression comments
            // $this->html .= "\n" . $this->bottomComment( $html, $this->html );
        }
    }

    protected function removeWhiteSpace( $str ) {
        $str = str_replace( "\t", ' ', $str );
        $str = str_replace( "\n",  '', $str );
        $str = str_replace( "\r",  '', $str );

        while ( stristr( $str, '  ' ) ) {
            $str = str_replace( '  ', ' ', $str );
        }

        return $str;
    }
}

add_action( 'get_header', __NAMESPACE__ . '\\html_compression_start' );

function html_compression_start() {
    ob_start( __NAMESPACE__ . '\\html_compression_finish' );
}

function html_compression_finish( $html ) {
    return new AI_HTML_Compression( $html );
}