<?php

namespace AITOM\DisableAttachmentPages;

/**
 * Disable Attachment Pages
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-disable-attachment-pages' );
 * After installation, you'll need to flush rewrite rules manually
 */

add_filter( 'rewrite_rules_array', __NAMESPACE__ . '\\remove_attachment_rewrites', 10, 1 );
add_filter( 'request', __NAMESPACE__ . '\\remove_attachment_query_var' );
add_filter( 'wp_unique_post_slug', __NAMESPACE__ . '\\remove_check_attachment_slug', 10, 6 );
add_filter( 'register_post_type_args', __NAMESPACE__ . '\\change_attachments_visibility', 20, 2 );
add_filter( 'attachment_link', __NAMESPACE__ . '\\change_attachment_link', 10, 2 );
add_action( 'template_redirect', __NAMESPACE__ . '\\redirect_attachment_pages' );

function remove_attachment_rewrites( $rules ) {
    foreach ( $rules as $pattern => $rewrite ) {
        if ( preg_match( '/([\?&]attachment=\$matches\[)/', $rewrite ) ) {
            unset( $rules[$pattern] );
        }
    }
    
    return $rules;
}

function remove_attachment_query_var( $vars ) {
    if ( ! empty( $vars['attachment'] ) ) {
        $vars['page'] = '';
        $vars['name'] = $vars['attachment'];
        
        unset( $vars['attachment'] );
    }

    return $vars;
}

function remove_check_attachment_slug( $slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug ) {
    global $wpdb, $wp_rewrite;

    if ( $post_type =='nav_menu_item' ) {
        return $slug;
    }
 
    if ( $post_type == "attachment" ) {

        $prefix = apply_filters( 'ai-filter-attachment-slug-prefix', 'wp-attachment-', $original_slug, $post_ID, $post_status, $post_type, $post_parent );

        if ( ! $prefix ) return $slug;

        // remove this filter and rerun with the prefix
        remove_filter( 'wp_unique_post_slug', __NAMESPACE__ . '\\remove_check_attachment_slug', 10 );
        $slug = wp_unique_post_slug( $prefix . $original_slug, $post_ID, $post_status, $post_type, $post_parent );
        add_filter( 'wp_unique_post_slug', __NAMESPACE__ . '\\remove_check_attachment_slug', 10, 6 );

        return $slug;
    }

    if ( ! is_post_type_hierarchical( $post_type ) ) {
        return $slug;
    }

    $feeds = $wp_rewrite->feeds;
    if( ! is_array( $feeds ) ) {
        $feeds = array();
    }

    /*
     * NOTE: This is the big change. We are NOT checking attachments along with our post type
     */
    $slug = $original_slug;
    $check_sql = "SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND post_type IN ( %s ) AND ID != %d AND post_parent = %d LIMIT 1";
    $post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $slug, $post_type, $post_ID, $post_parent ) );

    /**
     * Filters whether the post slug would make a bad hierarchical post slug.
     *
     * @since 3.1.0
     *
     * @param bool   $bad_slug    Whether the post slug would be bad in a hierarchical post context.
     * @param string $slug        The post slug.
     * @param string $post_type   Post type.
     * @param int    $post_parent Post parent ID.
     */
    if ( $post_name_check || in_array( $slug, $feeds ) || 'embed' === $slug || preg_match( "@^($wp_rewrite->pagination_base)?\d+$@", $slug )  || apply_filters( 'wp_unique_post_slug_is_bad_hierarchical_slug', false, $slug, $post_type, $post_parent ) ) {
        $suffix = 2;
        do {
            $alt_post_name = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
            $post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $alt_post_name, $post_type, $post_ID, $post_parent ) );
            $suffix++;
        } while ( $post_name_check );
        $slug = $alt_post_name;
    }

    return $slug;
}

function change_attachments_visibility( $args, $post_type ) {
        if ( $post_type == 'attachment' ) {
            $args['public'] = false;
            $args['publicly_queryable'] = false;
        }
    
        return $args;
    }
 
function change_attachment_link( $url, $id ) {
    $attachment_url = wp_get_attachment_url( $id );
    
    if ( $attachment_url ) return $attachment_url;
    
    return $url;
}
 
function redirect_attachment_pages() {
    if ( is_attachment() ) {
        if ( !empty( $url = wp_get_attachment_url( get_the_ID() ) ) ) {
            wp_redirect( $url, 301 );
            die;
        }
    }
}