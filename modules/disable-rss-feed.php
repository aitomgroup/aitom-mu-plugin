<?php

namespace AITOM\DisableRssFeed;

/**
 * Disable all WordPress RSS feed
 *
 * You can enable this feature by adding:
 * add_theme_support( 'ai-disable-rss-feed' );
 */

add_action( 'do_feed', __NAMESPACE__ . '\\disable_rss_feed', 1 );
add_action( 'do_feed_rdf', __NAMESPACE__ . '\\disable_rss_feed', 1 );
add_action( 'do_feed_rss', __NAMESPACE__ . '\\disable_rss_feed', 1 );
add_action( 'do_feed_rss2', __NAMESPACE__ . '\\disable_rss_feed', 1 );
add_action( 'do_feed_atom', __NAMESPACE__ . '\\disable_rss_feed', 1 );
add_action( 'do_feed_rss2_comments', __NAMESPACE__ . '\\disable_rss_feed', 1 );
add_action( 'do_feed_atom_comments', __NAMESPACE__ . '\\disable_rss_feed', 1 );

function disable_rss_feed() {
    wp_die( sprintf( __( 'No feed available, please visit the <a href="%s">homepage</a>!', 'AITOM-MU' ), esc_url( home_url( '/' ) ) ) );
}