<?php

/**
 * Plugin Name: AITOM
 * Description: Basic configuration of AITOM enviroment.
 * Version: 2.0.1
 * Author: AITOM
 * Author URI: https://www.aitomdigital.com/
 */

namespace AITOM;

// CML Authentication
require __DIR__ . '/soap/authentication.php';

// Load plugin helpers
require __DIR__ . '/libs/helpers.php';

// Load WP setup
require __DIR__ . '/libs/setup.php';

// Load tracy debug
if ( defined('WP_ENV') && WP_ENV == 'development' ) require __DIR__ . '/tracy/wp-tracy.php';

// Load plugin translations
add_action( 'plugins_loaded', function() {
    load_muplugin_textdomain( 'AITOM-MU', basename( dirname(__FILE__) ) . '/ai-i18n' );
} );

// Disallow indexing site on non-production environments.
if ( WP_ENV !== 'production' && !is_admin() ) {
    add_action( 'pre_option_blog_public', '__return_zero' );
}

// Disable WordPress core updates on stage and production server
if ( defined('DISALLOW_FILE_MODS') && DISALLOW_FILE_MODS && defined('WP_ENV') && WP_ENV != 'development' ) {
    add_filter( 'pre_site_transient_update_core', '__return_null' );
}

// Loading common modules according to theme support
// etc. add_theme_support( 'ai-html-compression' ) load module and common functions
add_action( 'after_setup_theme', function() {
    global $_wp_theme_features;

    // Load default login url
    if ( !isset( $_wp_theme_features['ai-default-login-url'] ) || !$_wp_theme_features['ai-default-login-url'] )
        require __DIR__ . '/libs/default-login.php';

    foreach ( [ 'clean-up.php', 'clean-walker.php', 'disable-asset-versioning', 'disable-attachment-pages.php', 'disable-rss-feed.php', 'disable-trackbacks.php', 'html-compression.php', 'jquery-cdn.php', 'js-to-footer.php', 'login-screen.php', 'nice-search.php', 'relative-urls.php', 'stage-switcher.php' ] as $fileName ) {
        $file = __DIR__ . '/modules/' . $fileName;

        $feature = 'ai-' . basename( $file, '.php' );

        if ( isset( $_wp_theme_features[ $feature] ) ) {

            if ( is_admin() && file_exists(  __DIR__ . '/modules/admin/' . $fileName ) ) require $file;

            if ( !is_admin() ) require $file;
        }
    }
}, 100);