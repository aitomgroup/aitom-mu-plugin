# AITOM MU Plugin

AITOM plugin pro WordPress jako MU (Must Use Plugins). Obsahuje sbírku modulů aplikovatelných na jakoukoliv šablonu a zajišťující jednotné vývojové prostředí.

## Požadavky

| Minimální verze | Jak zkontrolovat | Jak nainstalovat                                |
| --------------- | ---------------- | ----------------------------------------------- |
| PHP >= 5.4.x | `php -v`         | [php.net](http://php.net/manual/en/install.php) |

## Instalace

Projekty na Bedrocku tento plugin již obsahují. Pokud jej potřebuješ nainstalovat na jiný projekt, můžeš použít Composer nebo administraci WordPressu.

### přes Composer

Do composer.json přidej:

```sh
"repositories": [
  {
    "type": "vcs",
    "url": "https://bitbucket.org/aitomgroup/aitom-mu-plugin.git"
  }
],
"require": {
  "aitomgroup/aitom-mu-plugin": "dev-master"
}
```

Plugin se stáhne a nainstaluje příkazem v command line (uvnitř serveru / vagrantu - vagrant ssh):

```sh
composer update
```

### přes administraci WordPressu 

1. Stáhni si [nejnovější zip](https://bitbucket.org/aitomgroup/aitom-mu-plugin/get/master.zip) tohoto repozitáře.
2. Stažený archiv rozbal a umísti do složky 'mu-plugins' v adresáři 'wp-content' (nebo pro Bedrock v adresáři 'app').
3. Plugin se aktivuje automaticky pokud projekt používá 'bedrock-autoloader.php'.

## Vlastnosti

* Tracy debug lišta pro development prostředí
* Vynucené zakázání indexace webu (meta norobots, nofollow) mimo produkční prostředí
* Zakázání aktualizací mimo development prostředí
* Přihlašování do administrace WordPressu přes AITOM CML

## Moduly

Plugin obsahují následující moduly, které lze do šablony implementovat pomocí funkce 'add_theme_support'. Použij dle uvážení, můžeš použít všechny moduly nebo jen některé.

**Vyčištění kódu**
```php
add_theme_support( 'ai-clean-up' );
```

**Vyčištění menu od zbytečných tříd, id, obalů**
```php
add_theme_support( 'ai-clean-walker' );
```
  
**Odebrání verze WP v URL adrese všech stylů a skriptů**
```php
add_theme_support( 'ai-disable-asset-versioning' );
```
  
**Zakázání trackbacks/pingbacks**
```php
add_theme_support( 'ai-disable-trackbacks' );
```
  
**Minifikace a komprese kódu**

(funguje mimo 'developlent' prostředí)
  
```php
add_theme_support( 'ai-html-compression' );
```
  
**Skrytí celé WP struktury**

(funguje pouze na Bedrock, mimo 'development' prostředí a s povolenou podporou 'ai-html-compression')
```php
add_theme_support( 'ai-hide-wp' );
```
  
**Nahrazení adresy knihovny jQuery na CDN s fallback**
```php
add_theme_support( 'ai-jquery-cdn' );
```
  
**Přesunutí všech skriptů z hlavičky do patičky**
```php
add_theme_support( 'ai-js-to-footer' );
```
  
**Nová adresa výsledků vyhledávání z '/?s=retezec'  na '/search/retezec/'**
```php
add_theme_support( 'ai-nice-search' );
```

**Deaktivace všech RSS kanálů**
```php
add_theme_support( 'ai-disable-rss-feed' );
```
  
**Vytvoření relativních odkazů**
```php
add_theme_support( 'ai-relative-urls' );
```

**Přihlašovací stránka s logem AITOM**
```php
add_theme_support( 'ai-login-screen' );
```

**Vrácení přihlašovací adresy wp_login_url na výchozí**

Plugin totiž přihlašovací adresu automaticky mění na /adminrs/
```php
add_theme_support( 'ai-default-login-url' );
```

**Menu pro přepínání prostředí v Admin liště**

(pro fukčnost je potřeba mít zadané konstanty ENVIRONMENTS a WP_ENV)
```php
add_theme_support( 'ai-stage-switcher' );
```

**Zakázání stránek mediálních souborů**
```php
add_theme_support( 'ai-disable-attachment-pages' );
```
(po instalaci je nutné kliknout na Uložit v Nastavení -> Trvalé odkazy)