��    ,      |  ;   �      �     �  
   �     �     �               .     ;     >     K  
   R  	   ]     g  |   j  M   �  	   5  
   ?  T   J     �  >   �     �  	   �     �     �          !     '     .     6     M     b     j     x  |   ~     �                           
   -     8     G  4  N     �
  
   �
     �
     �
     �
     �
     �
                    &  	   -     7  �   :  k   �     A  
   M  f   X     �  `   �     5     >     G     L     [     q     w     ~     �     �     �     �     �  �   �     n     t     �     �     �     �     �     �     �                                            &       #      )   $           '                          ,             	                 
                     !   (   %          *               "         +        Allcaps Call from: Current DB query Current Post object Current Queried object Current Query request Current User DB Display Name E-mail First Name Func Call ID Image dimensions are too big. Please resize your image to maximum dimensions %dx%dpx. Uploaded image dimensions are %dx%dpx. Image size is too big. Recommended size is %dKB. Uploaded image size is %dKB. Last Name Last Query Log-in to administration as a <strong>superadmin</strong>. Use your AITOM CML login. Login No feed available, please visit the <a href="%s">homepage</a>! Page Now Parameter Post Queried Object Queries: %d, time: %s Query Query: Request Required MySQL Version Required PHP Version Rewrite Rewrite rules Roles This is the development environment, you may use this login:<br>Username: <strong>%s</strong>, Password: <strong>%s</strong> Time: TinyMCE Version User Value WP WP DB Version WP Version WordPress info search Project-Id-Version: AITOM MU 1.0.6
POT-Creation-Date: 2017-06-01 15:11+0200
PO-Revision-Date: 2017-06-01 15:14+0200
Last-Translator: 
Language-Team: AITOM <info@aitomdigital.com>
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-SearchPath-0: .
 Oprávnění Call from: Current DB query Current Post object Current Queried object Current Query request Aktuální uživatel DB Veřejně zobrazovat jako E-mail Jméno Func Call ID Rozměry obrázku jsou příliš velké. Zmenšete prosím váš obrázek do maximálního rozlišení %dx%dpx. Rozměry nahraného obrázku jsou %dx%dpx. Velikost obrázku je příliš velká. Doporučená velikost je %dKB. Velikost nahraného obrázku je %dKB. Příjmení Last Query Přihlášení do administrace jako <strong>superadmin</strong>. Použij svůj přístup do AITOM CML. Uživatelské jméno K dispozici není žádný kanál, vraťte se prosím na <a href=“%s”>titulní stránku</a>! Page Now Parametr Post Queried Object Queries: %d, time: %s Query Query: Request Požadovaná verze  MySQL Požadovaná verze PHP Rewrite Rewrite rules Role Toto je vývojové prostředí, kde můžeš použít tyto přihlašovací údaje:<br>Uživatelské jméno: <strong>%s</strong>, Heslo: <strong>%s</strong> Time: Verze TinyMCE User Hodnota WP Verze WP DB Verze WP Informace o WordPressu hledat 