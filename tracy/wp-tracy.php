<?php

/**
 * Tracy Debugger for WordPress
 *
 * Inspired by https://github.com/ktstudio/wp-tracy/
 *
 * Debugger is activated automatically on development environment.
 */


if ( !class_exists( 'Tracy\Debugger' ) ) return;

add_action( 'init', function() {

    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
        return; // for IE compatibility WordPress media upload
    }

    if ( defined( 'WP_TRACY_CHECK_USER_LOGGED_IN') && WP_TRACY_CHECK_USER_LOGGED_IN && is_user_logged_in() ) {
        return; // cancel for anonymous users
    }

    require __DIR__ . '/wp-tracy-base.class.php';

    Tracy\Debugger::enable( defined( 'WP_TRACY_DISABLE' ) ? WP_TRACY_DISABLE : false );

    // panels in the correct order
    $defaultPanels = [
        'WpTracyVers',
        'WpTracyUser',
        'WpTracyPost',
        'WpTracyQuery',
        'WpTracyQueriedObject',
        'WpTracyDb',
        'WpTracyRewrite'
    ];

    if (defined('SAVEQUERIES') && SAVEQUERIES) {
        $defaultPanels[] = 'WpTracyQueriesMonitor';
    }

    foreach ( $defaultPanels as $k => $panel ) {
        $panel_file = __DIR__ . '/panels/' . strtolower( $panel ) . '.class.php';

        if ( file_exists( $panel_file ) ){
            require $panel_file;

            $defaultPanels[$k] = 'WpTracy\\' . $panel;
        }  else {
            unset( $defaultPanels[$k] );
        }
    }

    $panels = apply_filters( 'ai-filter-panels', $defaultPanels );

    // panels registration
    foreach ( $panels as $className ) {
        Tracy\Debugger::getBar()->addPanel( new $className );
    }

    // disabled session cache of mySQL
    if ( !is_admin() ) {
        global $wpdb;
        $wpdb->query( 'SET SESSION query_cache_type = 0;' );
    }

}, 1 );

if ( !function_exists( 'wp_dump' ) ) {
    function wp_dump( $array ) {
        Tracy\Debugger::barDump( $array );
    }
}

add_action( 'admin_head', 'ai_wp_tracy_style' );
add_action( 'wp_footer', 'ai_wp_tracy_style' );

function ai_wp_tracy_style() { ?>
<style class=\"tracy-debug\">
    #tracy-debug .tracy-panel .nette-inner {
        max-height: 60vh;
        overflow: auto;
        max-width: 60vw;
    }
</style>
<?php }