<?php

namespace WpTracy;

/**
 * Custom panel based on global $wp_rewrite variable
 */

class WpTracyRewrite extends WpTracyBase {

    public function getTab() {
        return parent::getSimpleTab( __( 'Rewrite', 'AITOM-MU' ) );
    }

    public function getPanel() {
        global $wp_rewrite;
        
        return parent::getObjectPanel( $wp_rewrite, __( 'Rewrite rules', 'AITOM-MU' ) );
    }

}
