<?php

namespace WpTracy;

/**
 * Custom panel based on global $wp_rewrite variable
 */

class WpTracyQueriesMonitor extends WpTracyBase {
    protected static $queryMonitor = [];

    public function getTab() {
        $monitor = self::queryMonitor();
        
        if ( !isset( $monitor['total-queries'] ) || $monitor['total-queries']  == '' || !isset( $monitor['total-time'] ) || $monitor['total-time'] == '' ) return '';
        
        self::$queryMonitor = $monitor;
        
        return parent::getSimpleTab(
            '<svg viewBox="0 0 2048 2048"><path fill="#21759b" d="M1024 896q237 0 443-43t325-127v170q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-170q119 84 325 127t443 43zm0 768q237 0 443-43t325-127v170q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-170q119 84 325 127t443 43zm0-384q237 0 443-43t325-127v170q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-170q119 84 325 127t443 43zm0-1152q208 0 385 34.5t280 93.5 103 128v128q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-128q0-69 103-128t280-93.5 385-34.5z"></path></svg> ' . ( ( $monitor['total-time'] >= 1.0 ) ? floor( $monitor['total-time'] ) . ' s' : number_format( ( $monitor['total-time'] * 1000 ), 1 ) . ' ms' ) . ' / ' . $monitor['total-queries']
        );
    }

    public function getPanel() {
        global $wpdb;
        $monitor = self::$queryMonitor;
        
        if ( !isset( $monitor['total-queries'] ) || $monitor['total-queries']  == '' || !isset( $monitor['total-time'] ) || $monitor['total-time'] == '' ) return '';
        
        $queries = [];
        
        foreach ( $wpdb->queries as $k => $q ) {
            $q[0] = trim( preg_replace( '/[[:space:]]+/', ' ', $q[0]) );
            
            $queries[$k] = '<ul style="list-style: none; margin: 0; padding: 0">';
            
                $queries[$k] .= '<li><strong>' . __( 'Time:', 'AITOM-MU' ) . '</strong> ' . $q[1] . '</li>';
            
                if ( isset($q[1]) )
                    $queries[$k] .= '<li><strong>' . __( 'Query:', 'AITOM-MU' ) . '</strong> ' . htmlentities( $q[0] ) . '</li>';
            
                if ( isset($q[2]) )
                    $queries[$k] .= '<li><strong>' . __( 'Call from:', 'AITOM-MU' ) . '</strong> ' . htmlentities( $q[2] ) . '</li>';

            $queries[$k] .= '</ul>' . "\n";
            
        }
        
        return parent::getTablePanel(
            $queries,
            sprintf( __( 'Queries: %d, time: %s', 'AITOM-MU' ), $monitor['total-queries'], ( ( $monitor['total-time'] >= 1.0 ) ? floor( $monitor['total-time'] ) . ' s' : number_format( ( $monitor['total-time'] * 1000 ), 3 ) . ' ms' ) )
        );
    }

    public function queryMonitor() {
        global $wpdb;

        $debug_queries  = '';
        
        if ( $wpdb->queries ) {
            $total_time = timer_stop( FALSE, 22 );
            $total_time = (float) str_replace( ',', '.', $total_time );
            $total_query_time = 0;

            foreach ( $wpdb->queries as $q ) {
                $total_query_time += $q[1];
            }
        }

        $php_time = $total_time - $total_query_time;
        $mysqlper = $total_query_time / $total_time * 100;
        $phpper   = $php_time / $total_time * 100;
        
        return [
            'total-queries' => count( $wpdb->queries ),
            'total-time' => $total_time,   
            'total-query-time' => $total_query_time,
            'php-time' => $php_time,
            'mysql-per' => $mysqlper,
            'php-per' => $phpper
        ];
    }
}
