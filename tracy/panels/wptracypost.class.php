<?php

namespace WpTracy;

/**
 * Custom panel based on global (WP) $post variable
 */

class WpTracyPost extends WpTracyBase {

    public function getTab() {
        global $post;
        
        if ( self::issetAndNotEmpty( $post ) )
            return parent::getSimpleTab( __( 'Post', 'AITOM-MU' ) );
        
        return null;
    }

    public function getPanel() {
        global $post;
        
        return parent::getObjectPanel( $post, __( 'Current Post object', 'AITOM-MU' ) );
    }

}
