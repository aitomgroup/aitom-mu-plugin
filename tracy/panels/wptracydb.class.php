<?php

namespace WpTracy;

/**
 * Custom panel based on global $wpdb variable
 */

class WpTracyDb extends WpTracyBase {

    public function getTab() {
        return parent::getSimpleTab( __( 'DB', 'AITOM-MU' ) );
    }

    public function getPanel() {
        global $wpdb;
        
        $output = parent::getTablePanel( [
            __( 'Last Query', 'AITOM-MU' ) => $wpdb->last_query,
            __( 'Func Call', 'AITOM-MU' ) => $wpdb->func_call
        ], __( 'Current DB query', 'AITOM-MU' ) );
        
        $output .= parent::getObjectPanel( $wpdb );
        
        return $output;
    }

}
