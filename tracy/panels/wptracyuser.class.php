<?php

namespace WpTracy;

use Tracy\Debugger;

/**
 * Custom panel based on global $wp_user variable
 * 
 * @author Martin Hlaváč
 * @link http://www.ktstudio.cz/
 */
class WpTracyUser extends WpTracyBase {

    public function getTab() {
        if ( is_user_logged_in() )
            return parent::getSimpleTab( __( 'User', 'AITOM-MU' ) );
        
        return null;
    }

    public function getPanel() {
        $output = null;
        
        if ( is_user_logged_in() ) {
            $currentUser = wp_get_current_user();
            
            $output = parent::getTablePanel( [
                __( 'ID', 'AITOM-MU' ) => $currentUser->ID,
                __( 'Login', 'AITOM-MU' ) => $currentUser->user_login,
                __( 'E-mail', 'AITOM-MU' ) => $currentUser->user_email,
                __( 'Display Name', 'AITOM-MU' ) => $currentUser->display_name,
                __( 'First Name', 'AITOM-MU' ) => $currentUser->first_name,
                __( 'Last Name', 'AITOM-MU' ) => $currentUser->last_name,
                __( 'Roles', 'AITOM-MU' ) => Debugger::dump( $currentUser->roles, true ),
                __( 'Allcaps', 'AITOM-MU' ) => Debugger::dump( $currentUser->allcaps, true )
            ], __( 'Current User', 'AITOM-MU' ) );
        }
        
        return $output;
    }

}
