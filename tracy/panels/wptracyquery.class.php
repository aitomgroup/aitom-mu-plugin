<?php

namespace WpTracy;

/**
 * Custom panel based on global $wp_query variable
 */

class WpTracyQuery extends WpTracyBase {

    public function getTab() {
        return parent::getSimpleTab( __( 'Query', 'AITOM-MU' ) );
    }

    public function getPanel() {
        global $wp_query;
        
        $output = '<h1>' . __( 'Current Query request', 'AITOM-MU' ) . '</h1>';
        
        if ( self::issetAndNotEmpty( $wp_query->request ) ) {
            $output .= parent::getTablePanel( [
                __( 'Request', 'AITOM-MU' ) => $wp_query->request
            ] );
        }
        
        $output .= parent::getObjectPanel( $wp_query );
        
        return $output;
    }

}
