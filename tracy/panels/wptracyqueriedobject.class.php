<?php

namespace WpTracy;

/**
 * Custom panel based on result of function get_queried_object()
 */

class WpTracyQueriedObject extends WpTracyBase {

    public function getTab() {
        $queriedObject = get_queried_object();
        
        if ( self::issetAndNotEmpty( $queriedObject ) )
            return parent::getSimpleTab( __( 'Queried Object', 'AITOM-MU' ) );
        
        return null;
    }

    public function getPanel() {
        return parent::getObjectPanel( get_queried_object(), __( 'Current Queried object', 'AITOM-MU' ) );
    }

}
