<?php

namespace WpTracy;

use Tracy,
    Tracy\IBarPanel,
    Tracy\Dumper;

/**
 * Common basic model for other WP panels 
 */

abstract class WpTracyBase implements IBarPanel {

    /**
     * (HTML) content of tab for panel
     * 
     * @param string $title
     * @param string $description
     * @param string $icon
     * @return string
     */
    public function getSimpleTab( $title = null, $description = null, $icon = null ) {
        $output = "<span" . ( self::issetAndNotEmpty( $description ) ? " title=\"$description\"" : "" ) . ">";
        
        if ( self::issetAndNotEmpty( $icon ) )
            $output .= "<img src=\"$icon\" /> ";
        
        if ( self::issetAndNotEmpty( $title ) ) 
            $output .= $title;
        
        $output .= "</span>";
        
        return $output;
    }
    
    /**
     * (HTML) table content of panel based on parameters array
     * 
     * @param array $params
     * @param string $title
     * @return string
     */
    public function getTablePanel( array $params, $title = null ) {
        $output = null;
        
        if ( self::issetAndNotEmpty( $title ) )
            $output .= "<h1>$title</h1>";
        
        $output .= "<div class=\"nette-inner\">";
        
            $output .= "<table>";
        
                $output .= "<thead>";
                    $output .= "<tr>";
                        $output .= "<th>" . __( 'Parameter', 'AITOM-MU' ) . "</th>";
                        $output .= "<th>" . __( 'Value', 'AITOM-MU' ) . "</th>";
                    $output .= "</tr>";
                $output .= "</thead>";
        
                foreach ( $params as $key => $value)  {
                    $output .= "<tr>";
                        $output .= "<td>$key:</td>";
                        $output .= "<td>$value</td>";
                    $output .= "</tr>";
                }
        
            $output .= "</table>";
        
        $output .= "</div>";
        
        return $output;
    }

    /**
     * (HTML) content of panel based on object 
     * 
     * @param mixed $object
     * @param string $title
     * @return string
     */
    public function getObjectPanel( $object, $title = null ) {
        $output = null;
        
        if ( self::issetAndNotEmpty( $title ) )
            $output .= "<h1>$title</h1>";
        
        $output .= "<div class=\"nette-inner\">";
        
            $output .= Dumper::toHtml( $object, [ Dumper::COLLAPSE => false ] );
        
        $output .= "</div>";
        
        return $output;
    }

    /**
     * Check if the value is assigned and if it's not empty
     * 
     * @param mixed $value
     * @return boolean
     */
    public static function issetAndNotEmpty( $value ) {
        return isset( $value ) && !empty( $value );
    }
}
