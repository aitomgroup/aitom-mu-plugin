<?php

namespace AITOM\Setup;

/**
 * Setup WordPress core
 */

add_filter( 'network_admin_url', __NAMESPACE__ . '\\fix_network_admin_url', 10, 1 );

function fix_network_admin_url( $url ) {
    if ( !defined('WP_BASE') ) return $url;
    
    if ( strpos( $url, WP_BASE . '/wp-admin/' ) == false ) $url = str_replace( 'wp-admin/', WP_BASE . '/wp-admin/', $url );
    
    return $url;
}

add_filter( 'admin_url', __NAMESPACE__ . '\\fix_admin_url', 10, 1 );

function fix_admin_url( $url ) {
    if ( !defined('WP_BASE') || !defined('WP_ENV') || WP_ENV == 'staging' ) return $url;
    
    if ( strpos( $url, WP_BASE . '/wp-admin/' ) == false && strpos( $url, 'admin-ajax.php' ) != false && (!is_multisite() || ( get_current_blog_id() == 1 && !SUBDOMAIN_INSTALL ) ) ) $url = str_replace( 'wp-admin/', WP_BASE . '/wp-admin/', $url );
    
    return $url;
}

/**
 * Performs the filename cleaning
 *
 * This function runs when files are being uploaded to the WordPress media 
 * library. It takes an array with the file information, cleans the filename
 * and sends the file information back to where the function was called from. 
 *
 * @param array The file information including the filename in $file['name'].
 * @return array The file information with the cleaned or original filename.
 */

add_action( 'admin_init', function() {
    add_filter( 'wp_handle_upload_prefilter', __NAMESPACE__ . '\\clean_file_names' );
               
    //if ( ! current_user_can( 'administrator') )
        add_filter( 'wp_handle_upload_prefilter', __NAMESPACE__ . '\\filter_upload_size' );
} );

function clean_file_names ( $file ) {
    if ( !isset( $file['name'] ) || $file['name'] == '' )
        return [ 'error' => "AITOM MU: Error while uploading file - empty file name" ];
        
    $path = pathinfo( $file['name'] );
    
    if ( !isset( $path['extension'] ) || $path['extension'] == '' )
        return [ 'error' => "AITOM MU: Unrecognized file extensions." ];
    
    $new_filename = preg_replace( '/.' . $path['extension'] . '$/', '', $file['name'] );
    
    $file['name'] = sanitize_title( $new_filename ) . '.' . $path['extension'];

    // Return cleaned file or input file if it didn't match
    return $file;
}

function filter_upload_size( $file ) {
    
    if ( !isset( $file['tmp_name'] ) || $file['tmp_name'] == '' )
        return [ 'error' => "AITOM MU: Error while uploading file - empty temp file name" ];
    
    // Check only images
    if ( !preg_match( '/image/', $file['type'] ) )
        return $file;
    
    // Let filter maximum values
    $maximum = apply_filters( 'ai-filter-image-max-dim', [ 'width' => '2500', 'height' => '2500', 'size' => '5120000' ] );

    $img = getimagesize( $file['tmp_name'] );
    
    $size = $file['size'];
    $width = $img[0];
    $height = $img[1];
    
    if ( $size > $maximum['size'] )
        return [ 'error' => sprintf( __( 'Image size is too big. Recommended size is %dKB. Uploaded image size is %dKB.', 'AITOM-MU' ), round( $maximum['size']/1024 ), round( $size/1024 ) ) ];
    elseif ( $width > $maximum['width'] || $height > $maximum['height'] )
        return [ 'error' => sprintf( __( 'Image dimensions are too big. Please resize your image to maximum dimensions %dx%dpx. Uploaded image dimensions are %dx%dpx.', 'AITOM-MU' ), $maximum['width'], $maximum['height'], $width, $height ) ];
    else
        return $file;
}