<?php

namespace AITOM\DefaultLogin;

/**
 * Change default login url to /adminrs
 */

global $ai_login_url;
$ai_login_url = network_home_url( '/adminrs/', is_ssl() || force_ssl_admin() ? 'https' : 'http' );

add_filter( 'login_url', __NAMESPACE__ . '\\superadmin_login_url', 10, 3 );

function superadmin_login_url( $login_url, $redirect, $force_reauth ) {
    global $ai_login_url;
    $login_url = $ai_login_url;
    
    if ( !empty( $redirect ) ) $login_url = add_query_arg( 'redirect_to', urlencode( $redirect ), $login_url );
    
    if ( $force_reauth ) $login_url = add_query_arg( 'reauth', '1', $login_url );

    return $login_url;
}

add_filter( 'logout_url', __NAMESPACE__ . '\\superadmin_logout_url', 10, 2 );

function superadmin_logout_url( $logout_url, $redirect ) {
    global $ai_login_url;
    $logout_url = $ai_login_url;
    
    $args = [ 'action' => 'logout' ];
    if ( !empty( $redirect ) ) $args['redirect_to'] = $redirect;
    
    $logout_url = add_query_arg( $args, $logout_url );
    $logout_url = wp_nonce_url( $logout_url, 'log-out' );
    
    return $logout_url;
}

add_filter( 'register_url', __NAMESPACE__ . '\\superadmin_register_url', 10, 1 );

function superadmin_register_url( $register_url ) {
    global $ai_login_url;
    
    return add_query_arg( 'action', 'register', $ai_login_url );
}

add_filter( 'lostpassword_url', __NAMESPACE__ . '\\superadmin_lostpassword_url', 10, 2 );

function superadmin_lostpassword_url( $lostpassword_url, $redirect ) {
    global $ai_login_url;
    $lostpassword_url = $ai_login_url;
    
    $args = [ 'action' => 'lostpassword' ];
    if ( !empty( $redirect ) ) $args['redirect_to'] = $redirect;
    
    return add_query_arg( $args, $ai_login_url );
}