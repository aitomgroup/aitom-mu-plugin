#!/bin/bash

CURRENT_DIR=$(pwd)
CERT_FILE=cacert.pem
CERT_URL=https://curl.se/ca/cacert.pem

curl $CERT_URL | while read line; do
	echo $line
done > $CURRENT_DIR/$CERT_FILE

echo "ALL DONE - NEW $CERT_FILE READY."
