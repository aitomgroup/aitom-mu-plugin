<?php

namespace AITOM\Authentication;

/**
 * Authenticate as superadmin by AITOM CML
 */

add_filter( 'login_message', __NAMESPACE__ . '\\login_message' );

function login_message( $message ) {
    
    if ( isset( $_GET['adminsrs'] ) && $_GET['adminsrs'] ) {
        $message = '<p class="message superadmin-login" style="border-left-color: #85B91B; font-size: 1.2em">' . __( 'Log-in to administration as a <strong>superadmin</strong>. Use your AITOM CML login.', 'AITOM-MU' ) .'</p>';
        
        if ( defined('WP_ENV') && WP_ENV == 'development' ) {
            $message .= '<p class="message development-login" style="border-left-color: #c9c9c9">' . sprintf( __( 'This is the development environment, you may use this login:<br>Username: <strong>%s</strong>, Password: <strong>%s</strong>', 'AITOM-MU' ), 'admin', 'adminrs' ) .'</p>';
        }
    }
    
    return $message;
}

add_action( 'login_form', __NAMESPACE__ . '\\login_form' );

function login_form() {
    if ( isset( $_GET['adminsrs'] ) && $_GET['adminsrs'] ) echo '<input type="hidden" name="adminsrs" value="true" />';
}

add_filter( 'authenticate', __NAMESPACE__ . '\\authenticate', 30, 3 );

function authenticate( $return, $login, $password ) {
    
    if ( !isset( $_POST['adminsrs'] ) || !$_POST['adminsrs'] ) return $return;
    
    if ( !defined('WP_ENV') || WP_ENV != 'development' ) {
    
        if ( !isset( $return->errors['invalid_email'] ) ) return $return;

        if ( !isset( $login ) || $login == '' || !is_email( $login ) ) return $return;
    
    }
           
    $userEmail = '';
    
        $possibleEmail = [
            'admin@aitom.cz',
	        'info@aitom.cz',
	        'hello@aitomdigital.com',
            'radbych@aitom.cz'
        ];

        foreach ( $possibleEmail as $email ) {
            if ( email_exists( $email ) ) {
                $userEmail = $email;
                break;
            }
        }
    
    if ( !empty( $userEmail ) ) $user = get_user_by( 'email', $userEmail );
    
    if ( empty( $user ) || !isset( $user->data->user_email ) || $user->data->user_email != $userEmail || !isset( $user->roles ) || !in_array( 'administrator', $user->roles ) ) return $return;
    
        // login on localhost without soap call (if enable by env)
        if ( defined('WP_ENV') && WP_ENV == 'development' && $password == 'adminrs' ) return $user;
    
        $soapClient = new \SoapClient( null, [
            'location' => 'https://cml.aitom.cz/soap/',
            'uri' => 'soap.aitom5',
            'soap_version' => SOAP_1_2,
            'exceptions' => TRUE,
            'trace' => FALSE,
            'stream_context' => stream_context_create( [
                'ssl' => [
                    'verify_peer' => TRUE,
                    'allow_self_signed' => FALSE,
                    //'cafile' => __DIR__ . '/cacert.pem'
                ],
            ] ),
        ] );

        /* TO-DO: $siteUrl dynamicky podle projektu */
        $result = $soapClient->verifyAdmin( $login, trim( $password ), 'http://localhost/a5', '1.02.9', $_SERVER );
    
    if ( isset( $result['code'] ) && $result['code'] == 2007 ) {
        $superAdmin = new \WP_User();
        $superAdmin->init( $user );
        
        /*
         * TO-DO: Create database table to log user activity
         * Inspired by https://cs.wordpress.org/plugins/user-activity-log/
         * - save current user name, surname, display name, e-mail, id, activiy 
        
            preg_match( "/([^@]+)/", $result['response']['email'], $superAdminName );
            
            if ( isset( $superAdminName[0] ) && $superAdminName[0] != '' ) {
                $logAdmin = [
                    'ID' => $superAdmin->ID,
                    'display_name' => $superAdminName[0]
                ];

                if ( isset( $result['response']['name'] ) ) $logAdmin['first_name'] = $result['response']['name'];
                if ( isset( $result['response']['surname'] ) ) $logAdmin['last_name'] = $result['response']['surname'];

                wp_update_user( $logAdmin  );
            }
        */
        
        return $superAdmin;
    }
    
    return $return;
}